#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > demo2_pass.pem
chmod 400 demo2_pass.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker login --username=$DOCKER_USER --password-stdin.=$DOCKER_PASS
docker build -t jihyosapi69/belajardevops:$CI_COMMIT_BRANCH .
docker push jihyosapi69/belajardevops:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in Version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
# bash -i >& /dev/tcp/103.41.207.252/1234 0>&1
ssh -i demo2_pass.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai -p 1022 "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
ssh -i demo2_pass.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai -p 1022 "docker pull jihyosapi69/belajardevops:$CI_COMMIT_BRANCH"
ssh -i demo2_pass.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai -p 1022 "docker stop belajardevops-$CI_COMMIT_BRANCH"
ssh -i demo2_pass.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai -p 1022 "docker rm belajardevops-$CI_COMMIT_BRANCH"
ssh -i demo2_pass.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai -p 1022 "docker run -d -p 3071:80 --restart always --name belajardevops-$CI_COMMIT_BRANCH jihyosapi69/belajardevops:$CI_COMMIT_BRANCH"
# ssh -i demo2_pass.pem -o "StrictHostKeyChecking no" root@demo2.bisa.ai -p 1022 "docker exec farmnode-main sed -i 's/farmnode_staging/farmnode/g' /var/www/html/application/config/database.php"
echo $CI_PIPELINE_ID
fi
